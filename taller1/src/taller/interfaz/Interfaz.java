package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{ 
		ArrayList jugadores=new ArrayList();
		agregaraJugadores(jugadores);
		int fil=0;
		int col=0;
		configurarTabla(fil, col);
		juego= new LineaCuatro(jugadores, fil, col);
		juego();	
	}
       //TODO
	
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{ 
		while(!juego.fin())
		{
			System.out.println("Dijite la columna de la jugada");
			int col=Integer.parseInt(sc.next());
			if(juego.registrarJugada(col)==false)
			{
				System.out.println("Jugada invalida");
			}
			int fil=0;;
			String[][] table=juego.darTablero();
			for(int i=0;i<table.length || fil==0;i++)
			{
				if(table[i][col].equals("___")!=true)
				{
					fil=i-1;
				}
			}
			juego.terminar(fil, col);
			imprimirTablero();
		}
		
		
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		int fil=0;
		int col=0;
		ArrayList jugadores=new ArrayList();
		agregar("La Maquina", "@", jugadores);
		System.out.println("Escriba un nombre");
		String nombre=sc.next();
		System.out.println("Escriba una marca");
		String marca=sc.next();
		boolean a =agregar(nombre, marca, jugadores);
		if(a==false)
		{
			System.out.println("Usuario o Simbolo invalido");
			empezarJuegoMaquina();
		}
		configurarTabla(fil, col);
		juego=new LineaCuatro(jugadores, fil, col);
		juegoMaquina();
		
		//TODO
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		while(juego.fin())
		{
			juego.cambiarTurno();
			System.out.println("Dijite la columna de la jugada");
			int col=Integer.parseInt(sc.next());
			if(juego.registrarJugada(col)==false)
			{
				System.out.println("Jugada invalida");
			}
			int fil=0;;
			String[][] table=juego.darTablero();
			for(int i=0;i<table.length || fil==0;i++)
			{
				if(table[i][col].equals("___")!=true)
				{
					fil=i-1;
				}
			}
			juego.terminar(fil, col);
			imprimirTablero();
			juego.cambiarTurno();
			int colAl=0;;
			juego.registrarJugadaAleatoria(colAl);
			int fil2=0;;
			for(int i=0;i<table.length || fil2==0;i++)
			{
				if(table[i][colAl].equals("___")!=true)
				{
					fil2=i-1;
				}
			}
			juego.terminar(fil2, colAl);
			imprimirTablero();
		}
		
		
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] table=juego.darTablero();
		int fil=table.length;
		int col=table[fil].length;
		for(int i=0;i<col;i++)
		{
			for(int j=0;j<fil;j++)
			{
				if(i==table.length)
				{
					System.out.println(table[j][i]);
				}
				else
				{
					System.out.print(table[j][i]+"|");
				}
			}
		}
	}
	public void agregaraJugadores(ArrayList pJugadores)
	{
		
		System.out.println("Escriba un nombre");
		String nombre=sc.next();
		System.out.println("Escriba una marca");
		String marca=sc.next();
		boolean a =agregar(nombre, marca, pJugadores);
		if(a==false)
		{
			System.out.println("Usuario o Simbolo invalido");
			agregaraJugadores(pJugadores);
		}
		System.out.println("Agregue el siguiente jugador");
		boolean terminar=false;
		while(!terminar)
		{
			System.out.println("0-Agregar otro jugador");
			System.out.println("1-Siguiente");
			int opt=Integer.parseInt(sc.next());
			if(opt ==0)
			{
				agregaraJugadores(pJugadores);
			}
			else if(opt==1)
			{
				terminar=true;
			}
		}
		
			
		
				
	}
	public boolean agregar(String pNombre, String pMarca, ArrayList pJugadores)
	{
		
		boolean agregado=true;
		for(int i=0;i<pJugadores.size() || !agregado;i++)
		{
			Jugador ju1=(Jugador)pJugadores.get(i);
			String nombre=ju1.darNombre();
			String marca=ju1.darSimbolo();
			if(nombre.equals(pNombre))
			{
				agregado=false;
			}
			if(marca.equals(pMarca))
			{
				agregado=false;
			}
		}
		if(agregado==true)
		{
			Jugador a=new Jugador(pNombre,pMarca);
			pJugadores.add(a);
		}
		return agregado;
	}
	public void configurarTabla(int pFil, int pCol)
	{
		boolean correcto=false;
		while(!correcto)
		{
			System.out.println("Escriba el numero de filas");
			int fil=Integer.parseInt(sc.next());
			System.out.println("Escriba el numero de columnas");
			int col=Integer.parseInt(sc.next());
			if(fil>=4 && col>=4)
			{
				pFil=fil;
				pCol=col;
				correcto=true;
			}
			else
			{
				System.out.println("Filas y columnas deben ser mayores o iguales a 4");
				configurarTabla(pFil, pCol);
			}
		}
	}
	
}
