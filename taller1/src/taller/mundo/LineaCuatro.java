package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria(int pCol)
	{
		int nFilas=tablero.length;
		int nColm=tablero[nFilas].length;
		Random rnd=new Random();
		int colJugada= (int) (rnd.nextDouble()*nColm);
		registrarJugada(colJugada);
		pCol=colJugada;
		//TODO
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		String marca="";
		boolean registrado=false;
		for(int i=0;i<jugadores.size();i++)
		{
			Jugador juga=(Jugador)jugadores.get(i);
			String nombre=juga.darNombre();
			if(nombre.equals(atacante));
			{
				marca=juga.darSimbolo();
			}
		}
		int nFilas=tablero.length;
		for (int j=0;j<nFilas || !registrado;j++)
		{
			if (tablero[j][col].equals("___"));
			{
				tablero[j][col]=marca;
				registrado=true;
			}
		}
		//TODO
		return registrado;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		String marca="";
		for(int i=0;i<jugadores.size();i++)
		{
			Jugador juga=(Jugador)jugadores.get(i);
			marca=juga.darSimbolo();

		}
		//caso vertical
		if(tablero[fil+1][col].equals(marca) && tablero[fil+2][col].equals(marca) && tablero[fil+3][col].equals(marca))  
		{
			finJuego= true;
		}
		//caso horizontal
		else if(tablero[fil][col-1].equals(marca) || tablero[fil][col+1].equals(marca))
		{
			for(int i=0;i<3 || !finJuego;i++)
			{
				if(tablero[fil][col-3+i].equals(marca) && tablero[fil][col-2+i].equals(marca) && tablero[fil][col-1+i].equals(marca) && tablero[fil][col+i].equals(marca))
				{
					finJuego=true;
				}
			}		
		}
		//caso diagonal
		else if( tablero[fil-1][col-1].equals(marca) || tablero[fil+1][col+1].equals(marca))
		{
			for(int j=0;j<3 || !finJuego;j++)
			{
				if(tablero[fil-3+j][col-3+j].equals(marca) && tablero[fil-2+j][col-2+j].equals(marca) && tablero[fil-1+j][col-1+j].equals(marca) && tablero[fil+j][col+j].equals(marca))
				{
					finJuego=true;
				}
			}
		}
		
		//TODO
		return fin();
	}
	public void cambiarTurno()
	{
		turno++;
		if(turno>jugadores.size())
		{
			turno=0;
		}
		Jugador jAtacante=(Jugador)jugadores.get(turno);
		atacante=jAtacante.darNombre();
	}
	



}

